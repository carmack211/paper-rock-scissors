package sdacademy.pl.rock_paper_scissors;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    ImageButton rockButton, paperButton, scissorsButton;

    ImageView rockImage, paperImage, scissorsImage;

    int humanScore, compScore = 0;

    TextView userScore, computerScore;

    Snackbar mySnackbar;

    String score;

    Boolean cofnij = false;

    Boolean couting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rockButton = (ImageButton) findViewById(R.id.rockButton);
        paperButton = (ImageButton) findViewById(R.id.paperButton);
        scissorsButton = (ImageButton) findViewById(R.id.scissorsButton);

        rockImage = (ImageView) findViewById(R.id.rockView);
        paperImage = (ImageView) findViewById(R.id.paperView);
        scissorsImage = (ImageView) findViewById(R.id.scissorsView);

        userScore = (TextView) findViewById(R.id.userScore);
        computerScore = (TextView) findViewById(R.id.computerScore);

        rockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = playTurn("rockButton");
                //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                userScore.setText(Integer.toString(humanScore));
                computerScore.setText(Integer.toString(compScore));
                validate();

            }
        });

        paperButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = playTurn("paperButton");
                //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                userScore.setText(Integer.toString(humanScore));
                computerScore.setText(Integer.toString(compScore));
                validate();
            }
        });

        scissorsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = playTurn("scissorsButton");
                //Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                userScore.setText(Integer.toString(humanScore));
                computerScore.setText(Integer.toString(compScore));
                validate();
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("SAVED_HUMAN_SCORE", humanScore);
        outState.putInt("SAVED_CPU_SCORE", compScore);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        humanScore = savedInstanceState.getInt("SAVED_HUMAN_SCORE");
        compScore = savedInstanceState.getInt("SAVED_CPU_SCORE");
        userScore.setText(String.valueOf(humanScore));
        computerScore.setText(String.valueOf(compScore));
    }

    private String playTurn(String playerChoice) {
        String computerChoice = "";
        Random random = new Random();
        int computerChoiceNumber = random.nextInt(3) + 1;
            if(computerChoiceNumber == 1){
                computerChoice = "rockButton";
            } else if (computerChoiceNumber == 2){
                computerChoice = "paperButton";
            } else if (computerChoiceNumber == 3){
                computerChoice = "scissorsButton";
            }

            if(computerChoice == "rockButton"){
                rockImage.setVisibility(View.VISIBLE);
                paperImage.setVisibility(View.INVISIBLE);
                scissorsImage.setVisibility(View.INVISIBLE);
            } else if(computerChoice == "paperButton"){
                paperImage.setVisibility(View.VISIBLE);
                rockImage.setVisibility(View.INVISIBLE);
                scissorsImage.setVisibility(View.INVISIBLE);
            } else if(computerChoice == "scissorsButton"){
                scissorsImage.setVisibility(View.VISIBLE);
                rockImage.setVisibility(View.INVISIBLE);
                paperImage.setVisibility(View.INVISIBLE);
            }

            if(computerChoice == playerChoice){
                score = "Drow. Nobady won! ";
                showSnackbar(score, R.color.yellow);
                return score;

            }else if(playerChoice == "rockButton" && computerChoice == "scissorsButton") {
                humanScore++;
                score = "Rock crushes scissors, You win!";
                showSnackbar(score, R.color.green);
                couting = true;
                return score;
            }else if(playerChoice == "rockButton" && computerChoice == "paperButton") {
                compScore++;
                couting = false;
                score = "Paper coveres rock, Computer win!";
                showSnackbar(score, R.color.red);
                return score;
            }else if(playerChoice == "scissorsButton" && computerChoice == "rockButton") {
                compScore++;
                couting = false;
                score = "Rock crushes scissors, Computer win!";
                showSnackbar(score, R.color.red);
                return score;
            }else if(playerChoice == "scissorsButton" && computerChoice == "paperButton") {
                humanScore++;
                score = "Scissors cut paper, You win!";
                showSnackbar(score, R.color.green);
                return score;
            }else if(playerChoice == "paperButton" && computerChoice == "rockButton") {
                humanScore++;
                couting = true;
                score = "Paper coveres rock, You win!";
                showSnackbar(score, R.color.green);
                return score;
            }else if(playerChoice == "paperButton" && computerChoice == "scissorsButton") {
                compScore++;
                couting = false;
                score = "Scissors cut paper, Computer win!";
                showSnackbar(score, R.color.red);
                return score;
            }
            else return "not sure";
    }

    public void validate() {
        if (humanScore == 5 || compScore == 5) {
            if (humanScore > compScore) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setTitle("Koniec rozgrywki! Gratulację! Wygrałeś! ")
                        .setMessage("Czy chcesz zagrać ponownie? ")
                        .setIcon(R.drawable.logo)
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                rockImage.setVisibility(View.INVISIBLE);
                                paperImage.setVisibility(View.INVISIBLE);
                                scissorsImage.setVisibility(View.INVISIBLE);
                                humanScore = 0;
                                compScore = 0;
                                userScore.setText("0");
                                computerScore.setText("0");
                            }   //getresources
                        }).setNegativeButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                }).show();

            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                alertDialogBuilder.setTitle("Koniec rozgrywki! Niestety, przegrałeś!")
                        .setMessage("Może rewanż? ")
                        .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                rockImage.setVisibility(View.INVISIBLE);
                                paperImage.setVisibility(View.INVISIBLE);
                                scissorsImage.setVisibility(View.INVISIBLE);
                                humanScore = 0;
                                compScore = 0;
                                userScore.setText("0");
                                computerScore.setText("0");
                            }
                        }).setNegativeButton("Wyjście", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                }).show();
            }
        }
    }

    public void showSnackbar(String message, int color){
        mySnackbar = Snackbar.make(findViewById(R.id.mainLayout), message, Snackbar.LENGTH_LONG);
        if(message!= "Drow. Nobady won! " && cofnij == false) {
            mySnackbar.setAction("Cofnij", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cofnij = true;
                        if (couting == true) {
                            humanScore--;
                            userScore.setText(String.valueOf(humanScore));
                        } else if (couting == false) {
                            compScore--;
                            computerScore.setText(String.valueOf(compScore));
                        }
                    }
                });
        }
        mySnackbar.show();
        View sbView = mySnackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(), color));
    }
}
